# What:
XDC attachments were failing
# When:
Jan 3, 2024
# Where:
Merge Portal
# Description:

XDC attachment requests failed because the crictl version installed in the Merge wgd daemon fell out
of sync with the version of cri-o installed on the XDC hosts. This resulted from recent OS upgrades
performed on the k8s cluster over break

# Immediate Resolution:

Update the crictl binary in the wgd containers as follows:

```
kubectl get pod -n mergev1-xdc | grep wgd | awk '{print $1}' | xargs -I{} kubectl exec -it -n mergev1-xdc {} -- microdnf install cri-tools -y
kubectl get pod -n mergev1-xdc | grep wgd | awk '{print $1}' | xargs -I{} kubectl exec -it -n mergev1-xdc {} -- mv -v /usr/local/bin/crictl /usr/local/bin/crictl.preserve
```

# Future Resolution / Outstanding Issues:

https://gitlab.com/mergetb/portal/services/-/issues/316
