# Foundry Container Tag (Incident #2)

### Date

10/2/2019
6:11am (all times are in PT)

### Responders

Ryan Goodfellow, Geoff Lawler

### Status

Resolved

### Summary

Development Foundry containers had been pushed to [docker.io](https://cloud.docker.com/u/mergetb/repository/docker/mergetb/foundry/general) using `latest` tags.  [Cogs](https://gitlab.com/mergetb/tech/cogs/blob/master/pkg/foundry.go#L17) code uses `latest` tag rather than semantic versioning.  This caused new materializations to pull the broken development foundry container. The broken foundry container caused rex to return errors because it was unable to make grpc connections.  This in turn caused materialization pipelines to stall with no node tasks.

After a working foundry (v0.1.10) container was pushed to latest, the experiment was rematerialized without issue.

Solution: [Merge Request](https://gitlab.com/mergetb/tech/cogs/merge_requests/22)

### Impact

Users were unable to materialize experiments.

### Root Causes

A non-production ready foundry tag was pushed to `latest` which is used for production.

### Trigger

User materialized new experiment


### Resolution

An older v0.1.10 container was built and pushed to docker.io as `latest`.

### Detection

A user alerted us to DNS issues in their experiment

## Action Items

Merge request submitted to fix this issue.  Other containers which are used in the merge infrastructure must also be checked for semantic versioning.

## Lessons Learned

Semantic versioning for containers.

### What went well

### What went wrong

### Where we got lucky

## Timeline

```
6:11am incident alert
7:26am new container is pushed
7:51am materialization is recycled and restarted
8:00am everything looks okay
8:30am user notes dns issues
8:45am user reattaches to fix dns issue
```

## Supporting information


## Attribution
> Template from: Betsy Beyer, Chris Jones, Jennifer Petoff, and Niall Richard Murphy. [“Site Reliability Engineering.”](https://landing.google.com/sre/book/chapters/postmortem.html).

