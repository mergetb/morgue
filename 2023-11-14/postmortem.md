## What:
VLAN ID exhaustion

## When: 
Nov 9, 2023 – Nov 14, 2023

## Where: 
Merge Portal

## Description:
Realizations started failing due to lack of VLAN ID availability on the xleaf switch.

VLAN IDs are needed on the switch anytime a link needs to be trunked out from the hypervisor. This happens either when (1) a link is mapped to VMs that are running on separate hypervisors, or (2) a link is being emulated and thus needs to be embedded out through the emu node.

## Immediate Resolution:
Relinquish old realizations
Update experiment models to remove shaped links when they are not actually needed

## Future Resolution / Outstanding Issues:
Extend VXLAN domains out to the hypervisor rather than trunking VLANs (https://gitlab.com/mergetb/portal/services/-/issues/312)
