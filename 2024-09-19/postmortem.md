## What:
 XDCs failed to properly attach/route to materializations

## When:
 September 19th, 2024

## Where:
 Mod Deter Portal, Mod Deter Facility

## Description:  
Monthly patching is a reality of operational maintenance, and we have a designated patch window for services. During that patch window, the moddeter ops host was patched and rebooted, and an ansible playbook was run to restart services within the Mod Deter facility infrastructure.

Users began to report inability to send IP traffic to their materialization nodes around 12pm on 9/19/24 and we immediately began to look into the issue to determine the root cause. Unfortunately there was no obvious issue or error in anything that we could see at a glance, and the “testuser” script was continuing to run without issues.

We noticed that there were zero logs in the portal-side “wireguard” service, so we restarted that to force the reconciler to “ensure” all existing configurations.

Despite a restart on the portal side, facility side tunnels did not appear to be working. Typically this is going to be some sort of networking disconnect caused by a service or reconciler that has been stuck for some reason. We restarted the FRR routing daemon and the canopy daemon/reconciler systems on all switches, and infrapod hosts. We determined that restarting the “mars-wireguard” service, which is the broker service for incoming wireguard vpn tunnels from the portal may also help.

----

## Immediate Resolution:
 restart all network-related services (frr, canopy, mars-wireguard)

## Final Resolution:
 Investigate further why the systems were hung up, which may require a deep dive into FRR, and potentially some kind of testing where infrastructure is updated after deployment to watch for issues.