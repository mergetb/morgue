# What:
XDCs could not be created for user cos356av
# When:
Dec 1, 2023
# Where:
Merge Portal
# Description:

User cos356av could not create an XDC. k8s logs showed that it tried to create the XDC, but failed
in ContainerCreateError. The failure was due to:

```
 Warning  Failed     28s (x12 over 3m6s)  kubelet            Error: relabel failed /var/lib/kubelet/pods/05a5947c-a4f5-4492-a80f-f3993e9760c4/volume-subpaths/mergev1-mergefs-pv-xdc/xdc/4: open /var/lib/kubelet/pods/05a5947c-a4f5-4492-a80f-f3993e9760c4/volume-subpaths/mergev1-mergefs-pv-xdc/xdc/4/exp0/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret/top_secret: file name too long
```

The user presumably had a script with a bug that created this hierarchical chain of directories. The
problem this created was due to k8s selinux relabeling, which presumably timed out due to the very
long tree.

# Immediate Resolution:

- Delete the directories
- Restart the XDC via k8s.

# Future Resolution / Outstanding Issues:

Selinux relabeling continues to bite us. Need to figure out how to disable it on XDC creation
