# What:
Servers on the Merge portal failing to respond to actions
# When:
March 3, 2024
# Where:
Merge Portal
# Description:

Merge uses a reconciler system to execute actions in the portal and on the sites. This is mostly
encapsulated in a reconciler package that all reconcilers use. The reconciler package watches
changes in keys in an etcd database to know when to hand out actions to waiting reconcilers. The
etcd client package itself is responsible for letting Merge know when a key was changed. For a long
time there was a bug whereby some key changes were not received. Or not acted on. It was not clear
exactly where the event which drives reconciliation was getting lost - maybe in etd, maybe in the
reconciler package.

So rather than wait on etcd to tell us when a key changed, we applied a temporary patch in which
Merge polled etcd directly for key changes. And much work was done on the reconcile package to
ensure events were received via etd watches. As part of this work, the reconcile package was made
multithreaded (as a defense against one reconciler locking up other reconcilers or other actions in
the same reconciler). Part of this update intr oduced priority queues to sort events for dispersal.

This patch was merged into the code base and deployed on March 3rd. The rollout went mostly as
expected. But there were occasional times where a reconciler would stop responding to key updates,
stop reconciling all actions. Looking into this, the development team noticed that the troublesome
reconcilers were acknowledging key updates, but failing to act on them.

The reconcile package update included a series of queues and priority queues for tracking which and
when reconcilers should be invoked. The team discovered a bug in the priority queue implementation
which would cause the queues to lock - no events were ever resolved. This would happen on startup of
the reconciler. (So the day the fix was deployed, the naive and effective “fix” for the problem was
to restart the locked reconciler.)

A bug fix for the lockup problem was written, merged, and deployed a few days after the incident.

More technical details are on the MR: https://gitlab.com/mergetb/tech/reconcile/-/merge_requests/21

# Immediate Resolution:

Restart locked services

# Future Resolution / Outstanding Issues:

Apply MR listed above.
