# What:
Materializations were failing
# When:
March 8, 2024
# Where:
ModDeter Site
# Description:

Materializations were failing because the management network was down. Without the management
network, services never do the tasks they are supposed to do.

# Immediate Resolution:

Restart all services on all nodes and switches, which will cause the network to re-initialize.

# Future Resolution / Outstanding Issues:

It’s possible that this is caused by an issue with running old cumulus linux versions on the
switches.

https://gitlab.com/mergetb/facility/mars/-/issues/101
