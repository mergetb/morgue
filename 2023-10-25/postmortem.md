# What:
Wireguard Instability
# When:
Oct 25 2023
# Where:
Merge Portal + ModDeter Site
# Description:
The wireguard reconciler on the mod-deter site was found to be non-responsive to requests to
create WG peer connections (aka, create attachments to XDCs).
The “non-responsive” reconciler is a known issue. Reconcilers watch a keyspace in the etcd
database to determine what work they have to do. Etcd watches occasionally become inactive,
which means reconcilers do not see updates in etcd that tell them there is new work to do.
The second problem here was that upon restarting the wireguard service, there is a very long
delay until outstanding tasks (XDC connections) are handled. Joe’s estimation was 30 minutes
to handle 600 connection requests (~1 per materialization). There is no immediate explanation
as to why this takes so long, or resolution to make it faster.
The third problem here is that occasionally, even after being restarted, the wireguard reconciler
fails to create the peer connection. There are logs about “interface not existing” and “interface
already existing” that result from the portal’s view and the site’s view of the wireguard
connections getting out of sync. Brian is working on a big group of wireguard fixes (both portal
side and site side) that should resolve this issue.
# Immediate Resolution:
- Restart wireguard service on the site
-
# Future Resolution / Outstanding Issues:
- Fix wireguard stability issues (Brian currently working on)
- Either fix the etcd watch issue, or roll out new site services that use the polling-based
approach rather than watch based approach (as we have already rolled out polling
reconcilers on the portal)
- Investigate why the wireguard service takes so long to process materializations. ~3 sec
per connection request is way too slow
