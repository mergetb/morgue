# What:
Failed Infrapod Creation
# When:
Oct 26 2023
# Where:
ModDeter Site
# Description:

The infrapod reconciler on the mod-deter site was failing to create infrapods. One infrapod is
created per materialization, and so this meant that new materializations were not getting created
successfully. Users saw them as stuck in a “Pending” state.

The issue was one of insufficient resources. Specifically, podman ran out of locks and thus could
not create new pods. This caused the infrapod service to crash. On restart, it continued to crash
because the pod it previously tried and failed to create was in a middling state that the service
was not prepared to deal with

# Immediate Resolution:

1. Increase `num_locks` in `/etc/containers/containers.conf` to a higher value. We bumped it from 4096 to 8192
2. Blow away old lock file: `rm /dev/shm/libpod_lock`
3. Renumber the locks: `podman system renumber`
4. Kill pods in a non-Running state:
```
podman pod ps | grep -v Running

# For all pods found: 
podman pod rm -f <pod>
```
5. Restart infrapod service: `systemctl restart mars-infrapod`

# Future Resolution / Outstanding Issues:

As we continue to push our infrapod server nodes with more and more materializations, we will
continue tripping on configuration values that are not sufficiently high.

We can continue to bump them up and move to larger scales, but eventually we will run out of
resources. Until then, we can modify the default configuration in the facility-install package with
the highest values that seem to work

We are working on updating the Mars codebase to support multiple infrapod servers. ModDeter has an
additional machine installed and cabled, which we can use once the Mars software is updated. That
will alleviate the pressure on the current single infrapod server by ~50%
