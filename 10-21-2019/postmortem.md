# Equinix Power Outage (10/22-10/23)

### Date

10/22/2019
5:36am (all times are in PT)

### Responders

Ryan Goodfellow, Geoff Lawler, Lincoln Thurlow, Joe Barnes

### Status

On Going

### Summary

Two scheduled power disruptions at equinix caused an extended outage of the dcomp testbed. 
Two separate power outages over the course of two days.

* Infrastructure nodes with redundant power supply units (PSU) were not redundantly connect across power distribution units (PDU) on different circuits.
* Infrastructure nodes are not enabled to turn on in bios after power event (default behavior is to stay powered off)
* Storage Nodes responsible for etcd data store for all dcomp were unable to boot due to misconfigured mount
* Both Site Nodes were powered off
* BMC For Storage Node 2 is non-operational

### Impact

Users were unable to interact with the dcomp site. Dcomptb offline.

### Trigger

* Scheduled Power outage at Equinix on the A3A1 Circuit.
* Unplugging redundant power to site0 & site1

### Root Causes


* Invalid /etc/fstab entries for our etcd datastore on stor[0:2] nodes


The invalid entry in `/etc/fstab` prevented the storage nodes from booting normally.  Nodes would boot into maintenance mode because they were unable to mount `/dev/nvme3n1` to `/etcd-data`.  In these cases, `nvme3n1` was being used by `mdadm` as a part of the `sled-raid` responsible for the `/var/img` mount which sends sled images from the storage nodes to the experiment nodes.

The issue is only triggered on a storage reboot, which this power outage was the first case.  On reboot, the storage node detects the nvme devices (4) in essentially a random order, and then providing labels in that order.  So on reboot, nvme0 may become nvme2 on the next reboot.  `mdadm` was using `UUID` for each disk, which allowed it to automatically adjust to the disk discovery and labelling.  Our `/etc/fstab` mounts however used `/dev/nvme3n1` rather than the specific `UUID`.  This caused an error during the mounting process when `mdadm` had already taken `/dev/nvme3n1` but `mount` was attempting to mount it as another volume.

* Deletion of site1's infrapods

A decision was made to move all of the infrastructure for dcomptb across multiple PDUs on different circuits.  While `Site0` and `Site1` were not effected by this power outage because they were on a circuit which was not slated to be shutoff, the move from a single PDU to multiple PDUs would prevent a similar issue from occurring in the future.

A graceful shutdown of `Site1` caused immediate loss of all infrastructure and experimenter network plumbing.


### Resolution

* Invalid /etc/fstab entries for our etcd datastore on stor[0:2] nodes

Running `mdadm --misc --query --detail /dev/md/storX:sled0` would show which `nvme` paritions were participating in the `RAID0` configuration. This along with the contents of `/etc/fstab` showed conflicting use of `/dev/nvme3n1`.  Based on how `md` removes individual partitions from showing up in `/dev/disk/by-uuid` we could detect which `nvme` partition should replace `/dev/nvme3n1` in `/etc/fstab`.  We then replaced the `LABEL` with the `UUID` to prevent disk discovery ordering from preventing the storage nodes from booting up.  Rebooting the storage nodes then fixed the mounting issue.

* Getting etcd applications back online

With etcd not detectable by Site0 and Site1, no commands would work, as they all rely on the etcd datastore.  On Site0, the first recourse was to bring back up the sled service which use the raid group.

Site0:

```
sudo service nex-proxy restart
sudo service rex-proxy restart
sudo service llama-proxy restart
```

Site0 is responsible for providing dhcp and dns for the dcomptb infrastructure.  Once the `nex-proxy` was back online, the dcomptb machines could again get leases and begin to communicate on the `172.30.0.0/16` network.

Site 1:

```
sudo service sledb-proxy restart
sudo service rex-proxy restart
sudo service rex restart
```

Site1 is responsible for all cog management.  Getting rex-proxy, and therefore rex back online was a top priority.  Sledb is the sledapi and without the backend communications to etcd, would prevent sled, and therefore disk imaging to work on dcomptb nodes.


* Deletion of site1's infrapods

Still in progress.

### Detection

Prometheus notifications on dcomptb's slack alert channel.

On October 22nd, 1st Event:
```
AlertManagerAPP 5:36 AM
[FIRING:91] InstanceDown (prometheus page)
https://prometheus.dcomptb.net/alerts
AlertManagerAPP 6:10 AM
[FIRING:80] InstanceDown (prometheus page)
https://prometheus.dcomptb.net/alerts
AlertManagerAPP 6:45 AM
[FIRING:78] InstanceDown (prometheus page)
https://prometheus.dcomptb.net/alerts
AlertManagerAPP 7:20 AM
[FIRING:2] InstanceDown (prometheus page)
https://prometheus.dcomptb.net/alerts
```

On October 22nd, 2nd Event:
```
AlertManagerAPP 9:38 PM
[FIRING:90] InstanceDown (prometheus page)
https://prometheus.dcomptb.net/alerts
AlertManagerAPP 10:13 PM
[FIRING:90] InstanceDown (prometheus page)
https://prometheus.dcomptb.net/alerts
AlertManagerAPP 10:48 PM
[FIRING:90] InstanceDown (prometheus page)
https://prometheus.dcomptb.net/alerts
AlertManagerAPP 11:23 PM
[FIRING:90] InstanceDown (prometheus page)
https://prometheus.dcomptb.net/alerts
AlertManagerAPP 11:58 PM
[FIRING:90] InstanceDown (prometheus page)
https://prometheus.dcomptb.net/alerts
AlertManagerAPP 12:33 AM
[FIRING:90] InstanceDown (prometheus page)
https://prometheus.dcomptb.net/alerts
AlertManagerAPP 1:08 AM
[FIRING:90] InstanceDown (prometheus page)
https://prometheus.dcomptb.net/alerts
AlertManagerAPP 1:43 AM
[FIRING:90] InstanceDown (prometheus page)
https://prometheus.dcomptb.net/alerts
AlertManagerAPP 2:18 AM
[FIRING:90] InstanceDown (prometheus page)
https://prometheus.dcomptb.net/alerts
AlertManagerAPP 2:53 AM
[FIRING:90] InstanceDown (prometheus page)
https://prometheus.dcomptb.net/alerts
AlertManagerAPP 3:28 AM
[FIRING:90] InstanceDown (prometheus page)
https://prometheus.dcomptb.net/alerts
AlertManagerAPP 4:03 AM
[FIRING:90] InstanceDown (prometheus page)
https://prometheus.dcomptb.net/alerts
AlertManagerAPP 4:38 AM
[FIRING:90] InstanceDown (prometheus page)
https://prometheus.dcomptb.net/alerts
```

## Action Items

* [Site0 NAT on reboot](https://gitlab.com/dcomptb/ops/issues/64)
* [Fix of dcomp infrastructure ansible (to use 3 partitions, not all 4](https://gitlab.com/dcomptb/ops/issues/74)
* [Inclusion of `/etcd-data` mount into infrastructure ansible (using 4 partition)](https://gitlab.com/dcomptb/ops/issues/75)
* [Wiki page to re-initialize Site1 infrastructure on outage.](https://gitlab.com/dcomptb/ops/blob/1200-members/documentation/infrapod-recovery.md)
* [Rewire Infrastructure across PDU](https://gitlab.com/dcomptb/ops/commit/dfc00d9df79888323379ad857f02071af8c01a8a)

## Lessons Learned

* Redundant power supplies across infrastructure
* Do not power off site1 whenever possible
* Power testing.  Shutting off PDU/circuits to verify configurations.
* Reboot machines after configuration to verify configuration works in case of power failure.

### What went well

### What went wrong

* Powering off site1.

It is possible to do a power cable shuffle in order to re-align site1's power across PDUs.


### Where we got lucky

## Timeline

## Supporting information



## Attribution
> Template from: Betsy Beyer, Chris Jones, Jennifer Petoff, and Niall Richard Murphy. [“Site Reliability Engineering.”](https://landing.google.com/sre/book/chapters/postmortem.html).